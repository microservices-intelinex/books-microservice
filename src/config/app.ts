import { registerAs, ConfigService } from '@nestjs/config';
import {
  APP_CONFIG,
  JWT_CONFIG,
  MICROSERVICE_CONFIG,
  MONGO_DB,
} from './constants';

interface IEnvAppConfig {
  httpPort: number;
}

export type IAppConfig = IEnvAppConfig & ConfigService;

export default registerAs(APP_CONFIG, () => ({
  httpPort: process.env.HTTP_PORT || 3000,
}));

export const defaultPagination = {
  limit: 100,
  page: 1,
};

export const mongoConfig = registerAs(MONGO_DB, () => ({
  uri: process.env.MONGODB_URI,
  user: process.env.MONGODB_USER,
  pass: process.env.MONGODB_PASSWORD,
  dbName: process.env.MONGODB_NAME,
  retryAttempts: 10,
  useNewUrlParser: true,
  useUnifiedTopology: true,
}));

export const microserviceConfig = registerAs(MICROSERVICE_CONFIG, () => ({
  brokers: [process.env.MICROSERVICE_BROKER],
  ssl: true,
  sasl: {
    mechanism: process.env.MICROSERVICE_MECHANISM,
    username: process.env.MICROSERVICE_USERNAME,
    password: process.env.MICROSERVICE_PASSWORD,
  },
}));
