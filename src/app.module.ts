import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { BooksModule } from './books/books.module';
import app, { microserviceConfig, mongoConfig } from './config/app';
import enviroments from './config/enviroments';
import validation from './config/validation';
import { MongooseModule } from '@nestjs/mongoose';
import { KAFKA_ID, MICROSERVICE_CONFIG, MONGO_DB } from './config/constants';
import { ClientsModule, Transport } from '@nestjs/microservices';

const ConfigModuleProvider = ConfigModule.forRoot({
  envFilePath: enviroments[process.env.NODE_ENV] || '.env',
  isGlobal: true,
  load: [app, mongoConfig, microserviceConfig],
  validationSchema: validation,
});

const MongooseConfig = MongooseModule.forRootAsync({
  imports: [],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return configService.get(MONGO_DB);
  },
});

@Module({
  imports: [
    BooksModule,
    MongooseConfig,
    ConfigModuleProvider,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
