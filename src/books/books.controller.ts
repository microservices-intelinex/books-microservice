import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Inject,
} from '@nestjs/common';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { ClientProxy } from '@nestjs/microservices';
import {
  ApiQueryAuthor,
  ApiQueryEditorial,
  ApiQueryISBN,
  ApiQueryLimit,
  ApiQueryPage,
  ApiQueryPublisherYear,
  ApiQuerytitle,
} from '../common/decorators/api-query.pagination';
import { PaginationPipe } from '../common/pipes/pagination.pipe';
import { Pagination } from '../common/interfaces/pagination.interface';
import {
  SearcherParamenter,
  SearchParametersPipe,
} from './pipes/search-paramenter.pipe';
import { BooksService } from './services/books.service';
import { ApiOperation } from '@nestjs/swagger';
import { KAFKA_ID } from 'src/config/constants';
import { BOOK_DELETED } from 'src/config/events';

@Controller('books')
export class BooksController {
  constructor(
    private readonly booksService: BooksService,
    @Inject(KAFKA_ID) private readonly kafka: ClientProxy,
  ) {}

  @ApiOperation({
    summary: 'Allows create a new book',
  })
  @Post()
  create(@Body() createBookDto: CreateBookDto) {
    return this.booksService.create(createBookDto);
  }

  @ApiOperation({
    summary:
      'Allows find books using a set of parameters(title, author,publisherYear, publicher:editorial) including pagination',
  })
  @ApiQueryLimit()
  @ApiQueryPage()
  @ApiQueryISBN()
  @ApiQuerytitle()
  @ApiQueryAuthor()
  @ApiQueryPublisherYear()
  @ApiQueryEditorial()
  @Get()
  findAll(
    @Query(PaginationPipe) pagination: Pagination,
    @Query(SearchParametersPipe) searcher: SearcherParamenter,
  ) {
    return this.booksService.findAll(pagination, searcher);
  }

  @ApiOperation({
    summary:
      'Allows find a set of books having in acount search a paramenter in the paramenters(title, author,publisherYear, publicher:editorial) entries',
  })
  @Get('searcher/:value')
  findOneBySearchBar(
    @Query(PaginationPipe) pagination: Pagination,
    @Param('value') value: string,
  ) {
    return this.booksService.findAllBySearchBar(pagination, value);
  }

  @ApiOperation({
    summary: 'Allows find a book by mongo ID',
  })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.booksService.findOne(id);
  }
  @ApiOperation({
    summary: 'Allows update a book by mongo ID',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBookDto: UpdateBookDto) {
    return this.booksService.update(id, updateBookDto);
  }

  @ApiOperation({
    summary:
      'Allows remove a book by mongo ID, and delete all the entries foreing with ratings',
  })
  @Delete(':id')
  async remove(@Param('id') id: string) {
    const book = await this.booksService.findOne(id);
    this.kafka.emit(BOOK_DELETED, { ISBN: book.ISBN });
    return this.booksService.remove(id);
  }
}
