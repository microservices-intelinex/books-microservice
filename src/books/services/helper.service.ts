import { Injectable } from '@nestjs/common';

@Injectable()
export class HelperService {
    getWhereList(object: object) {
        return Object.entries(object).map(([k, v]) => ({ [k]: v }));
      }
}
