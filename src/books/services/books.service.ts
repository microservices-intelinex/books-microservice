import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { Pagination } from 'src/common/interfaces/pagination.interface';
import { HelperService } from './helper.service';
import { CreateBookDto } from '../dto/create-book.dto';
import { Book, BookDocument } from '../entities/book.entity';
import { SearcherParamenter } from '../pipes/search-paramenter.pipe';
import { UpdateBookDto } from '../dto/update-book.dto';
import { buildPagination } from 'src/common/pagination';
@Injectable()
export class BooksService {
  constructor(
    @InjectModel(Book.name) private bookModel: Model<BookDocument>,
    private helperService: HelperService,
  ) {}
  create(createBookDto: CreateBookDto) {
    return this.bookModel.create(createBookDto);
  }

  async findAll(pagination: Pagination, searcher: SearcherParamenter) {
    const total = await this.bookModel.count(searcher);
    const currentPage = (pagination.page - 1) * pagination.limit;
    const data = await this.bookModel
      .find(searcher)
      .limit(pagination.limit)
      .skip(currentPage);
    return buildPagination(
      data,
      pagination.page,
      pagination.limit,
      total,
      currentPage,
    );
  }

  async findAllBySearchBar(pagination: Pagination, searcher: string) {
    let where: any = {
      $or: [
        { ISBN: searcher },
        { title: searcher },
        { author: searcher },
        { publisher: searcher },
      ],
    };
    if (!isNaN(parseInt(searcher))) {
      where.$or.push({
        publishedYear: parseInt(searcher),
      });
    }
    where = !!searcher ? where : { where: {} };
    const total = await this.bookModel.count(where);
    const currentPage = (pagination.page - 1) * pagination.limit;
    const data = await this.bookModel
      .find(where)
      .limit(pagination.limit)
      .skip(currentPage);
    return buildPagination(
      data,
      pagination.page,
      pagination.limit,
      total,
      currentPage,
    );
  }

  async findOne(id: string) {
    const value = await this.bookModel.findOne({
      _id: new mongoose.Types.ObjectId(id),
    });
    if (!value) {
      throw new NotFoundException("Resource didn't find");
    }
    return value;
  }

  async update(id: string, updateBookDto: UpdateBookDto) {
    const result = await this.bookModel.findOneAndUpdate(
      { _id: new mongoose.Types.ObjectId(id) },
      updateBookDto,
    );
    return Object.assign(result, updateBookDto);
  }

  remove(id: string) {
    return this.bookModel.deleteOne({ _id: new mongoose.Types.ObjectId(id) });
  }
}
