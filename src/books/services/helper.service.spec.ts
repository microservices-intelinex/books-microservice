import { Test, TestingModule } from '@nestjs/testing';
import { HelperService } from './helper.service';

describe('HelperService', () => {
  let service: HelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HelperService],
    }).compile();

    service = module.get<HelperService>(HelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('METHOD: getWhereList Should return a list of object with one property from a objert', () => {
    const result = service.getWhereList({
      name: 'Juan',
      age: 12,
      lastname: 'pedro',
    });

    expect(result).toEqual([
      { name: 'Juan' },
      { age: 12 },
      { lastname: 'pedro' },
    ]);
  });
});
