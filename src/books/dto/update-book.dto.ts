import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, IsUrl } from 'class-validator';

export class UpdateBookDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  ISBN: string;
  @ApiProperty()
  @IsOptional()
  @IsString()
  title: string;
  @ApiProperty()
  @IsOptional()
  @IsString()
  author: string;
  @ApiProperty()
  @IsOptional()
  @IsNumber()
  publishedYear: number;
  @ApiProperty()
  @IsOptional()
  @IsString()
  publisher: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  imgUrlS: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  imgUrlM: string;
  @ApiProperty()
  @IsUrl()
  @IsOptional()
  imgUrlL: string;
}
