import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString, IsUrl } from 'class-validator';
import { Document } from 'mongoose';

export type BookDocument = Book & Document;

@Schema()
export class Book {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop({
    unique: true,
  })
  ISBN: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop()
  author: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Prop()
  publishedYear: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop()
  publisher: string;
  @ApiProperty()
  @IsUrl()
  @IsNotEmpty()
  @Prop()
  imgUrlS: string;
  @ApiProperty()
  @IsUrl()
  @IsNotEmpty()
  @Prop()
  imgUrlM: string;
  @ApiProperty()
  @IsUrl()
  @IsNotEmpty()
  @Prop()
  imgUrlL: string;

  @Prop({
    default: new Date(),
  })
  createdAt?: Date;
}

export const BookSchema = SchemaFactory.createForClass(Book);
