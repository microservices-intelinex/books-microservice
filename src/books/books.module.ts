import { Module } from '@nestjs/common';
import { BooksController } from './books.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Book, BookSchema } from './entities/book.entity';
import { HelperService } from './services/helper.service';
import { BooksService } from './services/books.service';
import { KAFKA_ID, MICROSERVICE_CONFIG, MONGO_DB } from '../config/constants';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

const MicroserviceConfig = ClientsModule.registerAsync([
  {
    name: KAFKA_ID,
    inject: [ConfigService],
    useFactory: (configService: ConfigService): any => {
      return {
        name: KAFKA_ID,
        transport: Transport.KAFKA,
        options: {
          client: configService.get(MICROSERVICE_CONFIG),
        },
      };
    },
  },
]);
@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Book.name,
        schema: BookSchema,
      },
    ]),
    MicroserviceConfig,
  ],
  controllers: [BooksController],
  providers: [BooksService, HelperService],
})
export class BooksModule {}
