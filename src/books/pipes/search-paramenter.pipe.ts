import {
  ArgumentMetadata,
  ForbiddenException,
  Injectable,
  Logger,
  PipeTransform,
} from '@nestjs/common';

export interface SearcherParamenter {
  ISBN?: string;
  title?: string;
  author?: string;
  publishedYear?: number;
  publisher?: string;
}

@Injectable()
export class SearchParametersPipe implements PipeTransform {
  transform(value: any, _metadata: ArgumentMetadata): SearcherParamenter {
    try {
      const searcher: SearcherParamenter = {};
      if (!value) {
        return searcher;
      }
      if (value.ISBN) {
        searcher.ISBN = value.ISBN;
      }
      if (value.title) {
        searcher.title = value.title;
      }
      if (value.author) {
        searcher.author = value.author;
      }
      if (value.publishedYear) {
        searcher.publishedYear = parseInt(value.publishedYear) ?? null;
      }
      if (value.publisher) {
        searcher.publisher = value.publisher;
      }
      return searcher;
    } catch (error) {
      new Logger(SearchParametersPipe.name).error(error);
      throw new ForbiddenException(error.message);
    }
  }
}
