import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, Observable, tap, throwError } from 'rxjs';

@Injectable()
export class ErrorHandlerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    Logger.log('Start request on bookServoce');
    return next.handle().pipe(
      tap((res: any) => {
        Logger.debug('End request ');
      }),
      catchError((e) => {
        Logger.debug('Error to answer:', e);
        return throwError(e);
      }),
    );
  }
}
