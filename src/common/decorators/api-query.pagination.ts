import { ApiQuery } from '@nestjs/swagger';

export const ApiQueryLimit = () =>
  ApiQuery({
    required: false,
    name: 'limit',
  });
export const ApiQueryPage = () =>
  ApiQuery({
    required: false,
    name: 'page',
  });
export const ApiQueryISBN = () =>
  ApiQuery({
    required: false,
    name: 'ISBN',
  });

export const ApiQuerytitle = () =>
  ApiQuery({
    required: false,
    name: 'title',
  });

export const ApiQueryAuthor = () =>
  ApiQuery({
    required: false,
    name: 'author',
  });

export const ApiQueryPublisherYear = () =>
  ApiQuery({
    required: false,
    name: 'publishedYear',
  });

export const ApiQueryEditorial = () =>
  ApiQuery({
    required: false,
    name: 'publisher',
  });
