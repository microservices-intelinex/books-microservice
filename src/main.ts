import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { IAppConfig } from './config/app';
import { APP_CONFIG } from './config/constants';
import { ConfigService } from '@nestjs/config';
import { ErrorHandlerInterceptor } from './common/interceptors/error-handler.interceptor';

async function bootstrap() {
  Logger.log('APP BOOKS STATING');
  const app = await NestFactory.create(AppModule);
  const config = app.get<IAppConfig>(ConfigService);
  app.enableCors({
    origin: 'http://localhost:3000',
  });
  app.useGlobalInterceptors(new ErrorHandlerInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  const configSwagger = new DocumentBuilder()
    .setTitle('Books api')
    .setDescription('This api allows tha management of the books')
    .setVersion('1.0')
    .addTag('')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('/api/v1/docs', app, document);

  await app.listen(config.get<IAppConfig>(APP_CONFIG).httpPort);
}
bootstrap();
